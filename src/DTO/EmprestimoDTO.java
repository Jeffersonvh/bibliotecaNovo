/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;

/**
 *
 * @author tads
 */
public class EmprestimoDTO implements Serializable {
    AlunoDTO aluno;
    LivroDTO livro;

    public EmprestimoDTO(AlunoDTO aluno, LivroDTO livro) {
        this.aluno = aluno;
        this.livro = livro;
    }

    public AlunoDTO getAluno() {
        return aluno;
    }

    public void setAluno(AlunoDTO aluno) {
        this.aluno = aluno;
    }

    public LivroDTO getLivro() {
        return livro;
    }

    public void setLivro(LivroDTO livro) {
        this.livro = livro;
    }

    
}
