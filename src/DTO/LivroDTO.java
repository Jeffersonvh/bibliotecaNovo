/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localDate;

/**
 *
 * @author tads
 */
public class LivroDTO implements Serializable {

    int codigoDeBarras;
    int idLivro;
    int exemplar;
    String dataAquisicaoExemplar;
    String dataCadastroSistema;
    String classificacao;
    String areaConhecimento;
    String autor;
    String titulo;
    String ano;
    Long isbn;
    String editora;
    int paginas;

    public LivroDTO() {

    }

    public LivroDTO(int codigoDeBarras, int idLivro, int exemplar, String dataAquisicaoExemplar, String dataCadastroSistema, String classificacao, String areaConhecimento, String autor, String titulo, String ano, long isbn, String editora, int paginas) {
        this.codigoDeBarras = codigoDeBarras;
        this.idLivro = idLivro;
        this.exemplar = exemplar;
        this.dataAquisicaoExemplar = dataAquisicaoExemplar;
        this.dataCadastroSistema = dataCadastroSistema;
        this.classificacao = classificacao;
        this.areaConhecimento = areaConhecimento;
        this.autor = autor;
        this.titulo = titulo;
        this.ano = ano;
        this.isbn = isbn;
        this.editora = editora;
        this.paginas = paginas;
    }

    public int getCodigoDeBarras() {
        return codigoDeBarras;
    }

    public void setCodigoDeBarras(int codigoDeBarras) {
        this.codigoDeBarras = codigoDeBarras;
    }

    public int getIdLivro() {
        return idLivro;
    }

    public void setIdLivro(int idLivro) {
        this.idLivro = idLivro;
    }

    public int getExemplar() {
        return exemplar;
    }

    public void setExemplar(int exemplar) {
        this.exemplar = exemplar;
    }

    public String getDataAquisicaoExemplar() {
        return dataAquisicaoExemplar;
    }

    public void setDataAquisicaoExemplar(String dataAquisicaoExemplar) {
        this.dataAquisicaoExemplar = dataAquisicaoExemplar;
    }

    public String getDataCadastroSistema() {
        return dataCadastroSistema;
    }

    public void setDataCadastroSistema(String dataCadastroSistema) {
        this.dataCadastroSistema = dataCadastroSistema;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public String getAreaConhecimento() {
        return areaConhecimento;
    }

    public void setAreaConhecimento(String areaConhecimento) {
        this.areaConhecimento = areaConhecimento;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

}
