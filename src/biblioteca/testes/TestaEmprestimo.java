/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca.testes;

import DTO.AlunoDTO;
import DTO.EmprestimoDTO;
import DTO.LivroDTO;
import dao.DaoBinario;
import java.util.HashMap;
import java.util.Map;
import static jdk.nashorn.internal.objects.NativeArray.map;
import static jdk.nashorn.internal.objects.NativeDebug.map;

/**
 *
 * @author tads
 */
public class TestaEmprestimo {

    public static void main(String[] args) {

        DaoBinario db = new DaoBinario();

        //------cria um novo aluno/usuario--------//
        AlunoDTO aluno1 = new AlunoDTO(1, "Jefferson");
        AlunoDTO aluno2 = new AlunoDTO(2, "Jorge");
        AlunoDTO aluno3 = new AlunoDTO(3, "Rodolfo");
        AlunoDTO aluno4 = new AlunoDTO(4, "Leopoldo");
        AlunoDTO aluno5 = new AlunoDTO(5, "Roberto");

        //------Busca um lirvo pelo código de barras-----//
        LivroDTO livro1 = db.getLivro("97473098");
        LivroDTO livro2 = db.getLivro("97483908");
        LivroDTO livro3 = db.getLivro("97379018");
        LivroDTO livro4 = db.getLivro("11111111");
        LivroDTO livro5 = db.getLivro("97473098");
        
        //------Empresta o exemplar se disponivel---------//
        db.emprestar(new EmprestimoDTO(aluno1, livro1));
        db.emprestar(new EmprestimoDTO(aluno2, livro2));
        db.emprestar(new EmprestimoDTO(aluno3, livro3));
        db.emprestar(new EmprestimoDTO(aluno4, livro4));
        db.emprestar(new EmprestimoDTO(aluno5, livro5));

        //------Busca um emprestimo pelo código do livro-----//
        EmprestimoDTO emprestado = db.getEmprestimo(97473098);

        System.out.println(emprestado);

        //-----Devolve o Livro-------///
        db.devolver(97473098);

        //----Testa se realmente devolveu------///
        emprestado = db.getEmprestimo(97473098);//retorno deve ser null
        System.out.println("--------");
        System.out.println(emprestado);
        System.out.println("--------");
        
        //----Relatório-----//
        HashMap<Integer, EmprestimoDTO> emprestados = db.getEmprestimos();
        System.out.println("Total de livros emprestado: " + emprestados.size());
        System.out.println("---------------------------------------");
        System.out.println("Obras emprestadas: ");

        for (Map.Entry<Integer, EmprestimoDTO> livro : emprestados.entrySet()) {
            System.out.println("Código de barras: " + livro.getKey());
            System.out.println("Titulo: " + livro.getValue().getLivro().getTitulo());
            System.out.println("Aluno: " + livro.getValue().getAluno().getNome());
            System.out.println("---------------------------------------");
        }

//        for (Object livro : emprestados.entrySet()) {
//            System.out.println(emprestados.get(livro) + " emprestado por "+ emprestados.get(livro));
//        }
    }
}
