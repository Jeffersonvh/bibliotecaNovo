/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import DTO.AlunoDTO;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

/**
 *
 * @author tads
 */
public class DaoALuno {
    private HashMap getAluno() {

        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "aluno.ser";

        HashMap retorno = new HashMap();
        FileOutputStream fout = null;

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(pastaDestino));
            retorno = (HashMap) in.readObject();
            in.close();

        } catch (Exception e) {
            System.out.println("Erro: " + e);
        }

        return retorno;
    }
    
    public AlunoDTO buscaAluno(String cod) {
        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "Biblioteca" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

        HashMap retorno = getAluno();

        return (AlunoDTO) retorno.get(cod);
    }
    
    public void gravaAluno(AlunoDTO emprestimo) {


            String path = System.getProperty("user.home");
            String barra = System.getProperty("file.separator");
            String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

            HashMap retorno = new HashMap();
            FileOutputStream fout = null;

            try {
                HashMap<Integer, AlunoDTO> alunos = getAluno();
                alunos.put(emprestimo.getMatricula(), emprestimo);
                ObjectOutputStream brhue = new ObjectOutputStream(new FileOutputStream(pastaDestino));
                brhue.writeObject(alunos);
                brhue.flush();
                brhue.close();
            } catch (Exception e) {
                System.out.println("Erro ao gravar aluno! Erro: " + e);
                return;
            }

            System.out.println("Aluno gravado com sucesso!");

    }
    
    
}
