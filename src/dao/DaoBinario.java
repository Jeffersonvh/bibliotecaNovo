/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import DTO.EmprestimoDTO;
import DTO.LivroDTO;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

/**
 *
 * @author tads
 */
public class DaoBinario {

    public HashMap getBinario() {

        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "teste.ser";

        HashMap retorno = new HashMap();
        FileOutputStream fout = null;

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(pastaDestino));
            retorno = (HashMap) in.readObject();
            in.close();

        } catch (Exception e) {
            System.out.println("Erro: " + e);
        }

        return retorno;
    }

    public LivroDTO getLivro(String cod) {
        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "Biblioteca" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

        HashMap retorno = getBinario();

        return (LivroDTO) retorno.get(cod);
    }

    public void emprestar(EmprestimoDTO emprestimo) {
        if(emprestimo.getAluno() == null || emprestimo.getLivro()==null){
            System.out.println("Livro ou aluno não cadastrados!");
            return;
        }
        if (emprestado(emprestimo.getLivro().getCodigoDeBarras()) == false) {

            String path = System.getProperty("user.home");
            String barra = System.getProperty("file.separator");
            String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

            HashMap retorno = new HashMap();
            FileOutputStream fout = null;

            try {
                HashMap<Integer, EmprestimoDTO> emprestimos = getEmprestimos();
                emprestimos.put(emprestimo.getLivro().getCodigoDeBarras(), emprestimo);
                ObjectOutputStream brhue = new ObjectOutputStream(new FileOutputStream(pastaDestino));
                brhue.writeObject(emprestimos);
                brhue.flush();
                brhue.close();
            } catch (Exception e) {
                System.out.println("Erro ao emprestar exemplar! Erro: " + e);
                return;
            }

            System.out.println("Livro emprestado com sucesso!");

        } else {
            System.out.println("Exemplar emprestado");
        }
    }

    public HashMap getEmprestimos() {

        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

        HashMap retorno = new HashMap();
        FileOutputStream fout = null;

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(pastaDestino));
            retorno = (HashMap) in.readObject();
            in.close();

        } catch (Exception e) {
            System.out.println("Erro ao buscar emprestimo: " + e);
        }

        return retorno;
    }

    public EmprestimoDTO getEmprestimo(Integer cod) {
        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

        HashMap retorno = getEmprestimos();

        return (EmprestimoDTO) retorno.get(cod);
    }

    private boolean emprestado(Integer cod) {
        if (getEmprestimo(cod) != null) {
            if (getEmprestimo(cod).getLivro().getCodigoDeBarras() == cod) {
                return true;
            }
        }
        return false;
    }

    public void devolver(Integer cod) {
        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "emprestimos.ser";

        FileOutputStream fout = null;
                
        try {
            HashMap<Integer, EmprestimoDTO> emprestimos = getEmprestimos();
            emprestimos.remove(cod);
            ObjectOutputStream brhue = new ObjectOutputStream(new FileOutputStream(pastaDestino));
            brhue.writeObject(emprestimos);
            brhue.flush();
            brhue.close();
        } catch (Exception e) {
            System.out.println("Erro ao devolver exemplar! Erro: " + e);
            return;
        }
        System.out.println("Livro devolvolvido!");
    }
}
