/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package migracao;

import DTO.LivroDTO;
import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import static java.lang.System.out;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tads
 */
public class daoImport {

    public static void iniciar() {
        try{
            escreverJson();
        }catch (IOException e){
            System.out.println("Erro ao tentar escrever no arquivo .JSON. Erro: "+e);
        }

//        try {
//            escreverBinario();
//        } catch (IOException ex) {
//            System.out.println("Erro ao tentar escrever no arquivo .SER. Erro: " + ex);
//        }
    }

    private static void escreverBinario() throws IOException {
        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "teste.ser";

        HashMap<String, LivroDTO> livros = leCSV();

        ObjectOutputStream brhue = new ObjectOutputStream(new FileOutputStream(pastaDestino));
        brhue.writeObject(livros);
        brhue.flush();
        brhue.close();
    }

    private static void escreverJson() throws IOException {
        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "teste.json";

        //JsonElement element = gson.toJsonTree(emptyMap, mapType);
    }

    private static HashMap<String, LivroDTO> leCSV() {

        String path = System.getProperty("user.home");
        String barra = System.getProperty("file.separator");
        String pastaDestino = path + barra + "NetBeansProjects" + barra + "BibliotecaBla" + barra + "bibliotecaNovo" + barra + "src" + barra + "arquivos" + barra + "importar.csv";

        HashMap<String, LivroDTO> livros = new HashMap();

        BufferedReader br = null;
        String linha = "";
        String csvDivisor = "\\|";
        try {

            br = new BufferedReader(new FileReader(pastaDestino));
            int i = 1;
            while ((linha = br.readLine()) != null) {

                if (i != 1) {
                    String[] info = linha.split(csvDivisor);
                    try {
                        livros.put(info[0], new LivroDTO(Integer.parseInt(info[0])/* codBarraLivro */, Integer.parseInt(info[1])/* idLivro */, Integer.parseInt(info[2])/* exemplar */, info[3]/*DataAquiEx*/, info[4]/*dataCadastroSistema*/, info[5]/*classificacao*/, info[6]/*AreaConhecimento*/, info[7]/*Autores*/, info[8]/*titulo*/, info[9]/*ano*/, Long.parseLong(info[10])/*isbn*/, info[11]/*editora*/, Integer.parseInt(info[12])/*paginas*/));
                    } catch (Exception e) {
                        System.out.println("Erro na linha " + i + ". Erro: " + e);
                    }
                }
                i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return livros;
    }
}
